$(document).ready(function(){
    $("#main-header").sticky({
    	topSpacing:0
    });

	var introHeight = $('#intro-section').height();
    $('.flexslider').flexslider({
		animation: "slide",
		prevText: "",
		nextText: "",
	});
	$('#intro-section li img').css('height', introHeight + 'px');
});