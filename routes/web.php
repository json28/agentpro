<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$breadcrumbs = [];
$breadcrumbs[] = array(
	'label'	=> 'Home',
	'link'	=> '/'
);
$communities = [
	array(
		'title' 		=> 'Desert Hot Springs',
		'slug'			=> 'desert-hot-springs',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Desert Hot Springs that Maxwell Mosey represents. This page will give you a good idea about what living in Desert Hot Springs is really like.'
	),
	array(
		'title' 		=> 'Palm Springs',
		'slug'			=> 'palm-springs',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Palm Springs that Maxwell Mosey represents. This page will give you a good idea about what living in Palm Springs is really like.'
	),
	array(
		'title' 		=> 'Sky Valley',
		'slug'			=> 'sky-valley',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Sky Valley that Maxwell Mosey represents. This page will give you a good idea about what living in Sky Valley is really like.'
	),
	array(
		'title' 		=> 'Rancho Santa Fe',
		'slug'			=> 'rancho-santa-fe',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Rancho Santa Fe that Maxwell Mosey represents. This page will give you a good idea about what living in Rancho Santa Fe is really like.'
	),
	array(
		'title' 		=> 'Rancho Mirage',
		'slug'			=> 'rancho-mirage',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Rancho Mirage that Maxwell Mosey represents. This page will give you a good idea about what living in Rancho Mirage is really like.'
	),
	array(
		'title' 		=> 'Palm Desert',
		'slug'			=> 'palm-desert',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Palm Desert that Maxwell Mosey represents. This page will give you a good idea about what living in Palm Desert is really like.'
	),
	array(
		'title' 		=> 'Indian Wells',
		'slug'			=> 'indian-wells',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in Indian Wells that Maxwell Mosey represents. This page will give you a good idea about what living in Indian Wells is really like.'
	),
	array(
		'title' 		=> 'La Quinta',
		'slug'			=> 'la-quinta',
		'content'	 	=> 'Step inside and take a virtual stroll through your new neighborhood! Following are the different sights to see in La Quinta that Maxwell Mosey represents. This page will give you a good idea about what living in La Quinta is really like.'
	)
];

// Route::get('/', function () {
//     return view('pages.home');
// });
Route::get('/', 'FehomeController@index');


// Redirects
Route::redirect('buyers', '/buyers/deciding-to-buy', 301);
Route::redirect('sellers', '/sellers/decide-to-sell', 301);

Route::prefix('buyers')->group(function () use ($breadcrumbs) {
	$breadcrumbs[] = array(
		'label'	=> 'Buyers',
		'link'	=> '/buyers'
	);

	Route::get('deciding-to-buy', function () use ($breadcrumbs) {
		$pageTitle = 'Deciding to Buy';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 1;

    	return view('pages.buyers.deciding-to-buy', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
    Route::get('preparing-to-buy', function () use ($breadcrumbs) {
		$pageTitle = 'Preparing to Buy';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 2;

    	return view('pages.buyers.preparing-to-buy', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
    Route::get('choosing-a-real-estate-agent', function () use ($breadcrumbs) {
		$pageTitle = 'Choosing a Real Estate Agent';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 3;

    	return view('pages.buyers.choosing-a-real-estate-agent', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
    Route::get('time-to-go-shopping', function () use ($breadcrumbs) {
		$pageTitle = 'Time to Go Shopping';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 4;

    	return view('pages.buyers.time-to-go-shopping', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
    Route::get('escrow-inspections-and-appraisals', function () use ($breadcrumbs) {
		$pageTitle = 'Escrow Inspections and Appraisals';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 5;

    	return view('pages.buyers.escrow-inspections-and-appraisals', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
    Route::get('moving-in', function () use ($breadcrumbs) {
		$pageTitle = 'Moving In';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$buyerStep = 6;

    	return view('pages.buyers.moving-in', compact('breadcrumbs', 'pageTitle', 'buyerStep'));
    });
});

Route::prefix('sellers')->group(function () use ($breadcrumbs) {
	$breadcrumbs[] = array(
		'label'	=> 'Sellers',
		'link'	=> '/sellers'
	);

	Route::get('decide-to-sell', function () use ($breadcrumbs) {
		$pageTitle = 'Decide to Sell';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 1;

    	return view('pages.sellers.decide-to-sell', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });

    Route::get('select-an-agent-and-price', function () use ($breadcrumbs) {
		$pageTitle = 'Select an Agent & Price';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 2;

    	return view('pages.sellers.select-an-agent-and-price', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });

    Route::get('prepare-to-sell', function () use ($breadcrumbs) {
		$pageTitle = 'Prepare to Sell';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 3;

    	return view('pages.sellers.prepare-to-sell', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });

    Route::get('accepting-an-offer', function () use ($breadcrumbs) {
		$pageTitle = 'Accepting an Offer';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 4;

    	return view('pages.sellers.accepting-an-offer', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });

    Route::get('escrow-inspections-and-appraisals', function () use ($breadcrumbs) {
		$pageTitle = 'Escrow Inspections and Appraisals';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 5;

    	return view('pages.sellers.escrow-inspections-and-appraisals', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });

    Route::get('close-of-escrow', function () use ($breadcrumbs) {
		$pageTitle = 'Close of Escrow';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$sellerStep = 6;

    	return view('pages.sellers.close-of-escrow', compact('breadcrumbs', 'pageTitle', 'sellerStep'));
    });
});

Route::prefix('communities')->group(function () use ($breadcrumbs, $communities) {
	$breadcrumbs[] = array(
		'label'	=> 'Communities',
		'link'	=> '/communities'
	);

	Route::get('/', function () use ($breadcrumbs, $communities) {
		$pageTitle = 'Communities';
		$breadcrumbs[1]['active'] = true;

		return view('archives.archive-communities', compact('breadcrumbs', 'pageTitle', 'communities'));
	});

	Route::get('{slug}', function ($slug) use ($breadcrumbs, $communities) {
		$community = array_first($communities, function($c) use ($slug) {
			return $c['slug'] == $slug;
		});

		if(! sizeof($community)) {
			return view('pages.404');
		}

		$pageTitle = $community['title'];
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);

		return view('singles.single-community', compact('breadcrumbs', 'pageTitle', 'community'));
	});
});

Route::get('/about', function () use ($breadcrumbs) {
	$pageTitle = 'About';
	$breadcrumbs[] = array(
		'label'	=> $pageTitle,
		'active'=> true
	);

    return view('pages.about', compact('breadcrumbs', 'pageTitle'));
});


// Route::get('/properties', function () use ($breadcrumbs) {
// 	$pageTitle = 'Properties';
// 	$breadcrumbs[] = array(
// 		'label'	=> $pageTitle,
// 		'active'=> true
// 	);

//     return view('pages.properties', compact('breadcrumbs', 'pageTitle'));
// });

Route::get('/properties', 'FepropertiesController@index');

Route::get('/contact-us', 'FecontactusController@index');

Route::get('/properties/{blogid}', 'FepropertiesController@propview');