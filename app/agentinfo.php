<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class agentinfo extends Model {
	use ObservantTrait;
	
    protected $table = 'agentinfo';

}
