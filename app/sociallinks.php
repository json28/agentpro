<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class sociallinks extends Model {
	use ObservantTrait;
	
    protected $table = 'sociallinks';

}
