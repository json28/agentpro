<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class properties extends Model {
	use ObservantTrait;
	
    protected $table = 'properties';

}
