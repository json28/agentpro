<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class pages extends Model {
	use ObservantTrait;
	
    protected $table = 'pages';

}
