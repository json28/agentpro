<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class blogs extends Model {
	use ObservantTrait;
	
    protected $table = 'blogs';

}
