<?php
namespace App\Observers;

use App\pages;

class pagesObserver
{
    
    /**
     * Listen to the pages creating event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function creating(pages $pages)
    {
        //code...
    }

     /**
     * Listen to the pages created event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function created(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages updating event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function updating(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages updated event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function updated(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages saving event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function saving(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages saved event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function saved(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages deleting event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function deleting(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages deleted event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function deleted(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages restoring event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function restoring(pages $pages)
    {
        //code...
    }

    /**
     * Listen to the pages restored event.
     *
     * @param  pages  $pages
     * @return void
     */
    public function restored(pages $pages)
    {
        //code...
    }
}