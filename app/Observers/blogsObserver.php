<?php
namespace App\Observers;

use App\blogs;

class blogsObserver
{
    
    /**
     * Listen to the blogs creating event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function creating(blogs $blogs)
    {
        //code...
    }

     /**
     * Listen to the blogs created event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function created(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs updating event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function updating(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs updated event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function updated(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs saving event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function saving(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs saved event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function saved(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs deleting event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function deleting(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs deleted event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function deleted(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs restoring event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function restoring(blogs $blogs)
    {
        //code...
    }

    /**
     * Listen to the blogs restored event.
     *
     * @param  blogs  $blogs
     * @return void
     */
    public function restored(blogs $blogs)
    {
        //code...
    }
}