<?php
namespace App\Observers;

use App\agentinfo;

class agentinfoObserver
{
    
    /**
     * Listen to the agentinfo creating event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function creating(agentinfo $agentinfo)
    {
        //code...
    }

     /**
     * Listen to the agentinfo created event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function created(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo updating event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function updating(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo updated event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function updated(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo saving event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function saving(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo saved event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function saved(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo deleting event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function deleting(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo deleted event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function deleted(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo restoring event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function restoring(agentinfo $agentinfo)
    {
        //code...
    }

    /**
     * Listen to the agentinfo restored event.
     *
     * @param  agentinfo  $agentinfo
     * @return void
     */
    public function restored(agentinfo $agentinfo)
    {
        //code...
    }
}