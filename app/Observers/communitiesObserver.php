<?php
namespace App\Observers;

use App\communities;

class communitiesObserver
{
    
    /**
     * Listen to the communities creating event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function creating(communities $communities)
    {
        //code...
    }

     /**
     * Listen to the communities created event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function created(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities updating event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function updating(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities updated event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function updated(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities saving event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function saving(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities saved event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function saved(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities deleting event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function deleting(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities deleted event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function deleted(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities restoring event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function restoring(communities $communities)
    {
        //code...
    }

    /**
     * Listen to the communities restored event.
     *
     * @param  communities  $communities
     * @return void
     */
    public function restored(communities $communities)
    {
        //code...
    }
}