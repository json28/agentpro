<?php
namespace App\Observers;

use App\sliders;

class slidersObserver
{
    
    /**
     * Listen to the sliders creating event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function creating(sliders $sliders)
    {
        //code...
    }

     /**
     * Listen to the sliders created event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function created(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders updating event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function updating(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders updated event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function updated(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders saving event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function saving(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders saved event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function saved(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders deleting event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function deleting(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders deleted event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function deleted(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders restoring event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function restoring(sliders $sliders)
    {
        //code...
    }

    /**
     * Listen to the sliders restored event.
     *
     * @param  sliders  $sliders
     * @return void
     */
    public function restored(sliders $sliders)
    {
        //code...
    }
}