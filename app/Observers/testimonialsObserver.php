<?php
namespace App\Observers;

use App\testimonials;

class testimonialsObserver
{
    
    /**
     * Listen to the testimonials creating event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function creating(testimonials $testimonials)
    {
        //code...
    }

     /**
     * Listen to the testimonials created event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function created(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials updating event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function updating(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials updated event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function updated(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials saving event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function saving(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials saved event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function saved(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials deleting event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function deleting(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials deleted event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function deleted(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials restoring event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function restoring(testimonials $testimonials)
    {
        //code...
    }

    /**
     * Listen to the testimonials restored event.
     *
     * @param  testimonials  $testimonials
     * @return void
     */
    public function restored(testimonials $testimonials)
    {
        //code...
    }
}