<?php
namespace App\Observers;

use App\properties;

class propertiesObserver
{
    
    /**
     * Listen to the properties creating event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function creating(properties $properties)
    {
        //code...
    }

     /**
     * Listen to the properties created event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function created(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties updating event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function updating(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties updated event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function updated(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties saving event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function saving(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties saved event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function saved(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties deleting event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function deleting(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties deleted event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function deleted(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties restoring event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function restoring(properties $properties)
    {
        //code...
    }

    /**
     * Listen to the properties restored event.
     *
     * @param  properties  $properties
     * @return void
     */
    public function restored(properties $properties)
    {
        //code...
    }
}