<?php
namespace App\Observers;

use App\sociallinks;

class sociallinksObserver
{
    
    /**
     * Listen to the sociallinks creating event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function creating(sociallinks $sociallinks)
    {
        //code...
    }

     /**
     * Listen to the sociallinks created event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function created(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks updating event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function updating(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks updated event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function updated(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks saving event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function saving(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks saved event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function saved(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks deleting event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function deleting(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks deleted event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function deleted(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks restoring event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function restoring(sociallinks $sociallinks)
    {
        //code...
    }

    /**
     * Listen to the sociallinks restored event.
     *
     * @param  sociallinks  $sociallinks
     * @return void
     */
    public function restored(sociallinks $sociallinks)
    {
        //code...
    }
}