<?php
namespace App\Observers;

use App\message;

class messageObserver
{
    
    /**
     * Listen to the message creating event.
     *
     * @param  message  $message
     * @return void
     */
    public function creating(message $message)
    {
        //code...
    }

     /**
     * Listen to the message created event.
     *
     * @param  message  $message
     * @return void
     */
    public function created(message $message)
    {
        //code...
    }

    /**
     * Listen to the message updating event.
     *
     * @param  message  $message
     * @return void
     */
    public function updating(message $message)
    {
        //code...
    }

    /**
     * Listen to the message updated event.
     *
     * @param  message  $message
     * @return void
     */
    public function updated(message $message)
    {
        //code...
    }

    /**
     * Listen to the message saving event.
     *
     * @param  message  $message
     * @return void
     */
    public function saving(message $message)
    {
        //code...
    }

    /**
     * Listen to the message saved event.
     *
     * @param  message  $message
     * @return void
     */
    public function saved(message $message)
    {
        //code...
    }

    /**
     * Listen to the message deleting event.
     *
     * @param  message  $message
     * @return void
     */
    public function deleting(message $message)
    {
        //code...
    }

    /**
     * Listen to the message deleted event.
     *
     * @param  message  $message
     * @return void
     */
    public function deleted(message $message)
    {
        //code...
    }

    /**
     * Listen to the message restoring event.
     *
     * @param  message  $message
     * @return void
     */
    public function restoring(message $message)
    {
        //code...
    }

    /**
     * Listen to the message restored event.
     *
     * @param  message  $message
     * @return void
     */
    public function restored(message $message)
    {
        //code...
    }
}