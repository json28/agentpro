<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class message extends Model {
	use ObservantTrait;
	
    protected $table = 'message';

}
