<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class communities extends Model {
	use ObservantTrait;
	
    protected $table = 'communities';

}
