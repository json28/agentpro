<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Serverfireteam\Panel\ObservantTrait;

class sliders extends Model {
	use ObservantTrait;
	
    protected $table = 'sliders';

}
