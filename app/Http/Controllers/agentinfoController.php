<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class agentinfoController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // * Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\agentinfo);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('phone', 'Phone');
			$this->grid->add('email', 'Email');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\agentinfo());

			$this->edit->label('Edit Agent Information');

			$this->edit->add('name', 'Name', 'text')->rule('required');
		
			$this->edit->add('phone', 'Phone', 'text')->rule('required');

			$this->edit->add('email', 'Email', 'text')->rule('required');

			$this->edit->add('broker_img', 'Broker image', 'image')->move('uploads/images/')->preview(80,80);

			$this->edit->add('agent_photo', 'Agent Photo', 'image')->move('uploads/images/')->preview(80,80);


        return $this->returnEditView();
    }    
}
