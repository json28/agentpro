<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FepropertiesController extends Controller
{
    public function index()
    {
    	$pageTitle = 'Properties';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$propert = DB::select('SELECT * FROM properties ORDER BY updated_at DESC');

    	return view('pages.properties', compact('breadcrumbs', 'pageTitle','propert'));

    }


    public function propview(Request $request)
    {
    	$pageTitle = 'Properties';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$slugs = explode('/',$request->path());
		// $slugs = preg_split("/[\s,]+/", $request->path());
		// var_dump($slugs);
		// die();
		$id = $slugs[1];

		$propert = DB::select('SELECT * FROM properties where id = "'. $id .'"');

    	return view('pages.propertiesview', compact('breadcrumbs', 'pageTitle','propert'));

    }
    
}
