<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FehomeController extends Controller
{
   
 public function index()
    {
        $slider = DB::select('SELECT path FROM sliders ORDER BY updated_at DESC');
        $message = DB::select('SELECT message FROM message ORDER BY updated_at DESC LIMIT 1')[0]->message;
        $community = DB::select('SELECT title FROM communities ORDER BY updated_at DESC LIMIT 1')[0]->title;
        $blog = DB::select('SELECT * FROM blogs ORDER BY updated_at DESC LIMIT 1');
        $testimonials = DB::select('SELECT * FROM testimonials ORDER BY updated_at DESC LIMIT 1');
        return view('pages.home', compact('slider', 'message', 'community','blog','testimonials'));
    }


}
