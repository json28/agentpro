<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class propertiesController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // * Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\properties);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('type', 'Type');
			$this->grid->add('status', 'Status');
			$this->grid->add('features', 'Features');
			$this->grid->add('price', 'Price');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\properties());

			$this->edit->label('Edit Properties');

			$this->edit->add('name', 'Name', 'text')->rule('required');

			$this->edit->add('photo', 'Photo', 'image')->move('uploads/images/')->preview(80,80);
		
			$this->edit->add('description', 'Description', 'redactor')->rule('required');

			$this->edit->add('price', 'Price', 'text')->rule('required');

			$this->edit->add('beds', 'Bed(s)', 'text')->rule('required');

			$this->edit->add('bath', 'Bath(s)', 'text')->rule('required');

			$this->edit->add('garage', 'Garage', 'text')->rule('required');

			$this->edit->add('type', 'Type', 'text')->rule('required');

			$this->edit->add('status', 'Status', 'text')->rule('required');

			$this->edit->add('features', 'Features', 'textarea')->rule('required');

			$this->edit->add('location', 'Location', 'textarea')->rule('required');
        
       
        return $this->returnEditView();
    }    
}
