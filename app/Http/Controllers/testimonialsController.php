<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class testimonialsController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // * Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\testimonials);
			$this->filter->add('title', 'Title', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('title', 'Title');
			$this->grid->add('author', 'Author');
			$this->grid->add('content', 'Content');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\testimonials());

			$this->edit->label('Edit Testimonials');
			$this->edit->add('title', 'Title', 'text');
			$this->edit->add('author', 'Author', 'text');
			$this->edit->add('content', 'Content', 'redactor')->rule('required');


        
       
        return $this->returnEditView();
    }    
}
