<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FecontactusController extends Controller
{
    public function index()
    {
    	$pageTitle = 'Contact Us';
		$breadcrumbs[] = array(
			'label'	=> $pageTitle,
			'active'=> true
		);
		$agentinfo = DB::select('SELECT * FROM agentinfo ORDER BY updated_at DESC LIMIT 1');

    	return view('pages.contact-us', compact('breadcrumbs', 'pageTitle','agentinfo'));

    }
}
