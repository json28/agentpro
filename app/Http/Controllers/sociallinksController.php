<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class sociallinksController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // * Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\sociallinks);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('link', 'Link');
			$this->grid->add('create_at', 'Date Created');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\sociallinks());

			$this->edit->label('Edit Social Link');

			$this->edit->add('name', 'Name', 'text')->rule('required');

			$this->edit->add('icon', 'Icon', 'image')->move('uploads/images/')->preview(80,80);
		
			$this->edit->add('link', 'Link', 'text')->rule('required');


        
       
        return $this->returnEditView();
    }    
}
