<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class messageController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // * Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\message);
			$this->filter->add('message', 'Message', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('message', 'Message');
			$this->grid->add('created_at', 'Date Created');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\message());

			$this->edit->label('Edit Intro Message');
		
			$this->edit->add('message', 'Intro Message', 'redactor')->rule('required');


        
       
        return $this->returnEditView();
    }    
}
