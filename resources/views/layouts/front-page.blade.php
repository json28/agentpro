@extends('app')
@section('template-content')
	@include('includes.header')
	<main class="home" id="content-wrapper">
		<section class="flexslider" id="intro-section">
			<ul class="slides">

				 @foreach ($slider as $sliders)
				 {!! $sliders->path !!}
                   <li>
						<img src="/images/slides/{!! $sliders->path !!}" />
				   </li>
                  @endforeach
			</ul>
			<div class="cta-area w3-cell w3-container">
				<a href="#">
					<span class="mid-border">
						<span class="inner-border">
							<em><label>BUY</label></em>
						</span>
					</span>
				</a>
				<a href="#">
					<span class="mid-border">
						<span class="inner-border">
							<em><label>SELL</label></em>
						</span>
					</span>
				</a>
				<a href="#">
					<span class="mid-border">
						<span class="inner-border">
							<em><label>SEARCH</label></em>
						</span>
					</span>
				</a>
			</div>
		</section>
		<section id="welcome-section">
			<div class="container">
				<div class="row ">
					<div class="col-md-9" id="welcome-content">
						<span class="welcome-text">Welcome to</span>
						<h4><span class="agent-name">Brian Kirchner</span> <span class="real-estate-text">Real Estate</span></h4>

						{!! $message !!}
						<!-- <p>It is with great pleasure that Brian Kirchner welcomes you to Southern California, and to his website. With years of experience in the market, he knows how crucial it is for you to find relevant, up-to-date information. The search is over. His website is designed to be your one-stop shop for real estate in Southern California.</p>
						<p>This is the moment that you should enjoy the most; looking at the available properties in Southern California; imagining yourself living in the home that you have always dreamed about. You don’t want just another database that gives you rehashed property descriptions. You want to walk around the neighborhood from the comfort of your own home. You want to get a clear picture about life in Southern California.</p> -->
						<p>
							<img src="/images/cb-logo.png">
						</p>
					</div>
					<div class="col-md-3" id="agent-photo">
						<img class="img-responsive" id="img_agent" src="/images/agent.png">
					</div>
				</div>
				<div class="row" id="quick-search">
					<h4>Quick Search</h4>
					<div class="quick-search-form col-md-12">
						<div class="form-row">
							<div class="col">
								<i class="fa fa-search"></i>
							</div>
							<div class="col-2">
								<input type="text" class="form-control" placeholder="Property Type">
							</div>
							<div class="col-2">
								<input type="text" class="form-control" placeholder="City or ZIP">
							</div>
							<div class="col-1">
								<input type="text" class="form-control" placeholder="Bedrooms">
							</div>
							<div class="col-1">
								<input type="text" class="form-control" placeholder="Bathrooms">
							</div>
							<div class="col-1">
								<input type="text" class="form-control" placeholder="Min Price">
							</div>
							<div class="col-1">
								<input type="text" class="form-control" placeholder="Max Price">
							</div>
							<div class="col-1">
								<button class="btn btn-default w-100 text-center">SEARCH</button>
							</div>
							<div class="col">
								<a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="featured-section">
			<div class="property">
				<div class="container">
					<div class="row">
						<div class="col-6">
							<h4 class="featured-title">Featured Property</h4>
							<div class="featured-info">
								<strong>$12,450,000</strong>
								<span>1700 East Walnut Avenue</span>
								<a class="btn btn-default" href="#">VIEW PROPERTY</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="community">
				<div class="container">
					<div class="row">
						<div class="offset-6 col-6">
							<h4 class="featured-title">Featured Community</h4>
							<div class="featured-info">
								<strong>{!! $community !!}</strong>
								<span></span>
								<a class="btn btn-default" href="#">VIEW COMMUNITY</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="cta-section">
			<div class="container">
				<div class="row">
					<div class="col cta-block blog">
						<h4>Blog</h4>
						@foreach ($blog as $blogs)
						<span>{!! $blogs->title !!}</span>
						<p>{!! $blogs->content !!}</p>
						<a href="#" class="read-more-btn">
							<div class="mid-circle">
								<div class="inner-circle">
									<i class="fa fa-plus"></i>
									<span>read more</span>
								</div>
							</div>
						</a>
						 @endforeach
					</div>
					<div class="col cta-block testimonials">
						<h4>Testimonials</h4>
						@foreach ($testimonials as $testimonial)
						<p>
							<span class="quote">"</span>
							{!! $testimonial->content !!}
							<span class="quote">"</span>
						</p>
						<span class="author">{!! $testimonial->author !!}</span>
						<a href="#" class="read-more-btn">
							<div class="mid-circle">
								<div class="inner-circle">
									<i class="fa fa-plus"></i>
									<span>read more</span>
								</div>
							</div>
						</a>
						 @endforeach
					</div>
					<div class="col cta-block areas">
						<h4>Areas</h4>
						<div class="row">
							<div class="col-6">
								<a href="#">La Quinta</a>
							</div>
							<div class="col-6">
								<a href="#">Indian Wells</a>
							</div>
							<div class="col-6">
								<a href="#">Palm Desert</a>
							</div>
							<div class="col-6">
								<a href="#">Rancho Mirage</a>
							</div>
							<div class="col-6">
								<a href="#">Rancho Santa Fe</a>
							</div>
							<div class="col-6">
								<a href="#">Sky Valley</a>
							</div>
							<div class="col-6">
								<a href="#">Palm Springs</a>
							</div>
							<div class="col-6">
								<a href="#">Desert Hot Springs</a>
							</div>
						</div>
						<a href="#" class="read-more-btn">
							<div class="mid-circle">
								<div class="inner-circle">
									<i class="fa fa-plus"></i>
									<span>read more</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
	</main>
	@include('includes.footer')
@endsection