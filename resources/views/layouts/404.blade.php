@extends('app')
@section('template-content')
	@include('includes.header')
	<main id="content-wrapper">
		<section class="content-container" id="fof-content">
			<div class="container fof-content">
				<div class="row">
					<div class="col-12">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<span>»</span>
								<li class="active">Error 404: Page not found</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="offset-2 col-4">
						<img src="/images/404-text-image.png">
					</div>
					<div class="offset-1 col-4">
						<img src="/images/404-image-right.png">
					</div>
				</div>
				<div class="row">
					<div class="mx-auto col-8">
						<p>Please check the webpage URL to make sure you have the correct address. It may also be possible that the content of this page is not yet ready for viewing. Feel free to revisit this page at another time or contact the website administrator for further assistance.</p>
					</div>
				</div>
				<div class="row">
					<div class="mx-auto col-8">
						<h4>Need Assistance?</h4>
						<form class="row">
							<div class="col-md-6">
								<input class="w-100" type="text" name="firstname" placeholder="First Name *">
								<input class="w-100" type="text" name="lastname" placeholder="Last Name *">
								<input class="w-100" type="email" name="email" placeholder="Email Address *">
								<input class="w-100" type="text" name="phone" placeholder="Phone Number *">
							</div>
							<div class="col-md-6"">
								<textarea class="w-100" name="message" placeholder="Your Message"></textarea>
								<button type="submit" class="w-100">SEND</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</main>
	@include('includes.footer')
@endsection