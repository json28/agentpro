@extends('app')
@section('template-content')
	@include('includes.header')
	<main id="content-wrapper">
		<section class="content-container">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						@if(sizeof($breadcrumbs) > 1)
						<div class="breadcrumbs">
							<ul>
								@foreach ($breadcrumbs as $breadcrumb)
									@if(isset($breadcrumb['active']))
										<li class="active">{{ $breadcrumb['label'] }}</li>
									@else
										<li><a href="{{ $breadcrumb['link'] }}">{{ $breadcrumb['label'] }}</a></li>
									@endif
									@if(! $loop->last)
										<span>»</span>
									@endif
								@endforeach
							</ul>
						</div>
						@endif
						@if(isset($pageTitle))
						<div class="page-title">
							<h2>{{ $pageTitle }}</h2>
						</div>
						@endif
						@yield('page-content')
					</div>
					<div class="col-md-3" id="content-sidebar">
						<div class="agent-image">
							<img class="w-100" src="/images/agent.png">
						</div>
						<div class="sidebar-info">
							<h2>Brian Kirchner</h2>
							<span>123.456.7890</span>
							<a href="#">agent@agentimage.com</a>
						</div>
						<div class="col-md-12" id="social-links">
							<ul class="social-links-list">
								<li class="social-item">
									<a href="#"><span><i class="fa fa-facebook"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-twitter"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-youtube"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-linkedin"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-google-plus"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-pinterest"></i></span></a>
								</li>
								<li class="social-item">
									<a href="#"><span><i class="fa fa-instagram"></i></span></a>
								</li>
							</ul>
						</div>
						<div class="col-md-12 cta-area">
							<a href="#">
								<span class="mid-border">
									<span class="inner-border">
										<em><label>BUY</label></em>
									</span>
								</span>
							</a>
							<a href="#">
								<span class="mid-border">
									<span class="inner-border">
										<em><label>SELL</label></em>
									</span>
								</span>
							</a>
							<a href="#">
								<span class="mid-border">
									<span class="inner-border">
										<em><label>SEARCH</label></em>
									</span>
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	@include('includes.footer')
@endsection