<header>
	<div class="container w3-container" id="social-links">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-links-list pull-right">
					<li class="social-item">
						<a href="#"><span><i class="fa fa-facebook"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-twitter"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-youtube"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-linkedin"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-google-plus"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-pinterest"></i></span></a>
					</li>
					<li class="social-item">
						<a href="#"><span><i class="fa fa-instagram"></i></span></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<section id="main-header">
		<div class="container w3-container">
			<div class="row">
				<div class="col-md-3" id="logo">
					<!-- <a href="#">Maxwell Mosey</a> -->
					<img class="w-100" src="/images/logo.png">
				</div>
				<div class="col-md-2" id="contact">
					<span><strong>123.456.7890</strong></span>
					<span>agent@agentimage.com</span>
				</div>
				<div class="col-md-7 pull-right">
					<div class="topnav" id="myTopnav">
						<ul class="pull-right" id="navigation">
							<li class="nav_li">
								<a href="/">
									<span>HOME</span>
								</a>
							</li>
							<li>
								<a href="/properties">
									<span>PROPERTIES</span>
								</a>
							</li>
							<li>
								<a href="/communities">
									<span>COMMUNITIES</span>
								</a>
							</li>
							<li>
								<a href="/buyers">
									<span>BUYERS</span>
								</a>
							</li>
							<li>
								<a href="/sellers">
									<span>SELLERS</span>
								</a>
							</li>
							<li>
								<a href="/about">
									<span>ABOUT</span>
								</a>
							</li>
							<li>
								<a href="/contact-us">
									<span>CONTACT</span>
								</a>
							</li>
							<li>
							<a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</header>