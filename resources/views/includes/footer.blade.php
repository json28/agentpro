<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<ul class="footer-navigation">
					<li><a href="#">HOME</a></li>
					<li><a href="#">PROPERTIES</a></li>
					<li><a href="#">COMMUNITIES</a></li>
					<li><a href="#">BUYERS</a></li>
					<li><a href="#">SELLERS</a></li>
					<li><a href="#">ABOUT</a></li>
					<li><a href="#">CONTACT</a></li>
				</ul>
				<p>COPYRIGHT © 2018 MAXWELL MOSEY. ALL RIGHTS RESERVED. SITEMAP. REAL ESTATE WEBSITE DESIGN BY <a href="#"><strong><u>AGENT IMAGE</u></strong></a></p>
				<p>
					<i class="ai-realtor"></i>
					<i class="ai-mls"></i>
					<i class="ai-eho"></i>
				</p>
			</div>
			<div class="col-md-5">
				<div class="row align-items-center">
					<div class="col" id="social-links">
						<ul class="social-links-list">
							<li class="social-item">
								<a href="#"><span><i class="fa fa-facebook"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-twitter"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-youtube"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-linkedin"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-google-plus"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-pinterest"></i></span></a>
							</li>
							<li class="social-item">
								<a href="#"><span><i class="fa fa-instagram"></i></span></a>
							</li>
						</ul>
					</div>
					<img class="footer-logo col-3" src="/images/cb-logo.png">
				</div>
			</div>
		</div>
	</div>
</footer>