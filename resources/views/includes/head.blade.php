<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Real Estate Agent Website."/>
	<link rel="canonical" href="http://www.realestate.com/" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Real Estate" />
	<meta property="og:description" content="Real Estate Agent Website" />
	<meta property="og:url" content="http:///www.realestate.com/" />
	<meta property="og:site_name" content="realestate" />
	<title>Real Estate Agent Website</title>
	
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3mobile.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="/css/properties_view.css">
	<link rel="stylesheet" type="text/css" href="/css/mobile_optimize.css">
	<!-- <link rel="stylesheet" href="{{ asset('css/properties_list.css') }}" /> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	
	<link rel="stylesheet" type="text/css" href="/vendors/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" id="aios-starter-theme-concatenated-google-fonts-css" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600%7CLato%3A400%2C700%7COpen+Sans%3A400%2C400italic%2C600%2C600italic%2C700%2C300italic%2C300%7CGilda+Display%7CRoboto%3A400%2C300%2C500%2C700%7CYellowtail%3A400%2C300%2C700%7COpen+Sans%3A400%2C600%2C700%2C800%2C300&amp;ver=4.7.6" type="text/css" media="all">
	<link rel='stylesheet' id='agentimage-font-css'  href='https://cdn.thedesignpeople.net/agentimage-font/fonts/agentimage.font.icons.css?ver=4.7.6' type='text/css' media='all' />
    <link rel="stylesheet" href="/vendors/flexslider/flexslider.css" />
    <script type="text/javascript" src="/js/navigation.js"></script>
</head>