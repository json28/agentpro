@extends('pages.sellers.sellers')
@section('sellers-content')
	<h3>Inspections and Appraisals</h3>

	<p>Most buyers will have the property inspected by a licensed property inspector within the time frame that was agreed upon in the effective contract to purchase. Some buyers will have several different inspectors inspect the property, if they wish to obtain professional opinions from inspectors who specialize in a specific area (eg. roof, HVAC, structure). If the agreement is conditional upon financing, then the property will be appraised by a licensed appraiser to determine the value for the lending institution via third party. This is done so that the lending institution can confirm their investment in your property is accurate. A buyer of a commercial property may also have a complete environmental audit performed and/or soil test, if required by the lending institution.</p>

	<h3>The Closing Agent.</h3>

	<p>Either a title company or an attorney will be selected as the closing agent, whose job is to examine and insure clear title to real estate. After researching the complete recorded history of your property, they will certify that 1) your title is free and clear of encumbrances (eg. mortgages, leases, or restrictions, liens) by the date of closing; and 2) all new encumbrances are duly included in the title.</p>

	<h3>Contingencies.</h3>

	<p>A contingency is a condition that must be met before a contract becomes legally binding. For instance, a buyer will usually include a contingency stating that their contract is binding only when there is a satisfactory home inspection report from a qualified inspector.</p>

	<p>Before completing his or her purchase of your property, the buyer goes over every aspect of the property, as provided for by purchase agreements and any applicable addendums. These include:</p>

	<ul>
		<li>Obtaining financing and insurance;</li>
		<li>Reviewing all pertinent documents, such as preliminary title reports and disclosure documents; and</li>
		<li>Inspecting the property. The buyer has the right to determine the condition of your property by subjecting it to a wide range of inspections, such as roof, termite/pest, chimney/fireplace, property boundary survey, well, septic, pool/spa, arborist, mold, lead based paint, HVAC, etc.</li>
	</ul>

	<p>Depending on the outcome of these inspections, one of two things may happen:</p>

	<p>1.	Either each milestone is successfully closed and the contingencies will be removed, bringing you one step closer to the closing; or</p>

	<p>2.	The buyer, after reviewing the property and the papers, requests a renegotiation of the terms of contract (usually the price).</p>

	<p>How do you respond objectively and fairly to the buyer when a renegotiation is demanded, while acting in your best interests? This is when a professional listing agent can make a real difference in the outcome of the transaction. Having dealt with various property sales in the past, we guarantee our expertise and total commitment to every customer, no matter what their situation is.</p>

	<h3>Loan Approval and Appraisal.</h3>

	<p>We suggest that you accept buyers who have a lender’s pre-approval, approval letter, or written loan commitment, which is a better guarantee of loan approval than a pre-qualification or no documentation from a lending institute. Expect an appraiser from the lender’s company to review your property and verify that the sales price is appropriate.</p>
@endsection