@extends('layouts.default')
@section('page-content')
	<div class="sellers-header row">
		<div class="col-2">
			@if($sellerStep == 1)
			<a class="active">
			@else
			<a href="/sellers/decide-to-sell">
			@endif
				<i class="ai-icon-1"></i>
				<span>decide to sell</span>
			</a>
		</div>
		<div class="col-2">
			@if($sellerStep == 2)
			<a class="active">
			@else
			<a href="/sellers/select-an-agent-and-price">
			@endif
				<i class="ai-icon-2"></i>
				<span>select an agent and price</span>
			</a>
		</div>
		<div class="col-2">
			@if($sellerStep == 3)
			<a class="active">
			@else
			<a href="/sellers/prepare-to-sell">
			@endif
				<i class="ai-icon-3"></i>
				<span>prepare to sell</span>
			</a>
		</div>
		<div class="col-2">
			@if($sellerStep == 4)
			<a class="active">
			@else
			<a href="/sellers/accepting-an-offer">
			@endif
				<i class="ai-icon-4"></i>
				<span>accepting an offer</span>
			</a>
		</div>
		<div class="col-2">
			@if($sellerStep == 5)
			<a class="active">
			@else
			<a href="/sellers/escrow-inspections-and-appraisals">
			@endif
				<i class="ai-icon-5"></i>
				<span>escrow inspections and appraisals</span>
			</a>
		</div>
		<div class="col-2">
			@if($sellerStep == 6)
			<a class="active">
			@else
			<a href="/sellers/close-of-escrow">
			@endif
				<i class="ai-icon-6"></i>
				<span>close of escrow</span>
			</a>
		</div>
	</div>

	@yield('sellers-content')
@endsection