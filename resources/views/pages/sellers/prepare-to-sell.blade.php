@extends('pages.sellers.sellers')
@section('sellers-content')
	<p>You can do a lot to increase the appeal of your property and to create a lasting impact on potential buyers.</p>

	<h3>What To Do To Prepare:</h3>

	<p>The following are a couple of factors to keep in mind when listing your property for sale:</p>

	<p>1. <strong>Curb Appeal.</strong></p>

	<p>Keeping your landscape pristine, and adding creative touches to your yard, such as colorful annuals, will create an immediate impact on passers-by and potential buyers.</p>

	<p>2. <strong>Property Repairs.</strong></p>

	<p>Simple upgrades such as window repairs, polishing the doorknobs, and a fresh coat of paint in the most frequently used rooms will instantly brighten up the property.</p>

	<p>3. <strong>Cleanliness and Staging.</strong></p>

	<p>Keep your property uncluttered, sweet-smelling and well-lit from top-to-bottom. Pay attention to details: put away the kitty litter, place a vase of fresh flowers near the entryway, pop a batch of cinnamon rolls in the oven, have your carpets cleaned. Your agent will scan the property before it is listed for sale to see how you can improve the staging of your property.</p>

	<p>4. <strong>Disclosures and Inspections.</strong></p>

	<p>We are very familiar with the legal procedures involved in disclosures and are ready to help you develop a thorough disclosure statement beneficial to both you and the buyer, as well as suggest home improvement measures before placing your property on the market (such as termite and pest inspections).</p>

	<p>5. <strong>Showtime.</strong></p>

	<p>Presenting your property to potential buyers is a job that we will take care of for you. Buyers feel more comfortable discussing the property with the agent, if you are not there. Moreover, your agent will know what information will be most useful in representing your interests when speaking with prospective buyers.</p>
@endsection