@extends('pages.sellers.sellers')
@section('sellers-content')
	<p>So you have decided to sell your property. Before anything else, it is a good idea to sit down and clarify your motivations and draw up a basic time frame for the selling process.</p>

	<h3>Why Sell?</h3>
	<p>Why do you want to sell your property? Do you intend to simply find a larger property, or do you plan on moving to another neighborhood, school district, city or state? You might think your reasons are obvious, but it would do well to consider the implications of each option for your lifestyle, opportunities, and finances. Being clear about your intentions for selling will make it easier for us to determine the most appropriate option for your specified financial, lifestyle, and real estate goals.</p>

	<h3>When Should I Sell?</h3>
	<p>You should immediately establish your time frame for selling. If you need to sell quickly, we can speed up the process by giving you a complete market analysis and action plan to obtain all of your goals. If there is no pressing need to sell immediately, you can sit down with one of our expert real estate agents to thoroughly review the current market conditions and find the most favorable time to sell.</p>

	<h3>What Is The Market Like?</h3>
	<p>When you work with us, you can be sure that you will have our knowledge, expertise and negotiating skills at work for you to arrive at the best market prices and terms. We will keep you up-to-date on what is happening in the marketplace and the price, financing, terms and conditions of competing properties. With us, you will know exactly how to price and when to sell your property.</p>

	<h3>How Do I Optimize My Finances?</h3>
	<p>Deciding to sell your property demands a serious consideration of your current financial situation and future possibilities. With the help of our qualified agents, you will be able to effectively assess the cumulative impact of these changes, estimate potential proceeds of selling your property, and plan effective tax savings and estate planning strategies. We will ensure that you not only take control of your finances, but use them to their fullest potential.</p>
@endsection