@extends('pages.sellers.sellers')
@section('sellers-content')
	<h3>The Price Is Not Always Right.</h3>
	<p>“The higher the price, the better the offer.” Do not let yourself be fooled by this popular misconception. Price is not always the determining factor when accepting an offer for several important reasons: the initial offer is usually not final, and there are a number of terms and conditions that may influence the final outcome of a price. You can trust our professionals to help you thoroughly evaluate every proposal without compromising your marketing position.</p>

	<h3>Negotiating The Right Way.</h3>
	<p>We take the ethical responsibility of fairly negotiating contractual terms very seriously. It is our job to find a win-win agreement that is beneficial to all parties involved. You may even have to deal with multiple offers before ratifying the one you judge to be the most suitable for you – and as your agents, we will guarantee a thorough and objective assessment of each offer to help you make the right choice.</p>

	<h3>The Initial Agreement and Deposit.</h3>
	<p>An effective agreement is a legal arrangement between a potential purchaser and the property’s seller. Laws vary from state to state, but in order to be a legally, binding agreement, the agreement may require consideration. This consideration (initial and additional deposit) is to be held in the closing agent’s escrow account pending the fulfillment of conditions or contingencies in the effective agreement.</p>

	<p>Some important tips to keep in mind to streamline the process even further:</p>

	<ul>
		<li><strong>Keep written records of everything.</strong><br/>
		For the sake of clarity, it will be extremely useful to transcribe all verbal agreements including counter-offers and addendums, and convert them to written agreements to be signed by both parties. We will assist you in drafting all the paperwork for your sale and make sure that you have copies of everything.</li>
		<li><strong>Stick to the schedule.</strong><br/>
		Now that you have chosen your offer, you and the buyer will be given a timeline to mark every stage in the process of closing the real estate contract. Meeting the requirements on time ensures a smoother flow of negotiations and also ensures that each party involved is not in breach of their agreements. During the process we will keep you constantly updated so you will always be prepared for the next step.</li>
	</ul>
@endsection