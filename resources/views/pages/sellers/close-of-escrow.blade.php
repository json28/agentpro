@extends('pages.sellers.sellers')
@section('sellers-content')
	<h3>Closing Day</h3>
	<p>If you have come this far, this means that it is almost time for a congratulations, but not yet. Do not forget to tie up these loose ends:</p>

	<h3>Final Walk-Through Inspection.</h3>
	<p>More of a formality than anything else, the final inspection takes place the day before, or the day of the closing. The buyer visits the property to verify that all is in working order, everything is the same as when the buyer last viewed the property, and that there are no extra items left behind.</p>

	<h3>Cancel Home Services and Utilities.</h3>
	<p>We will provide a list of useful numbers for the termination of home services and utilities after the closing occurs.</p>

	<h3>Be Prepared.</h3>
	<p>We are ready to assist you should an unforeseen glitch pop up, even at this last stage. If something at the property breaks down or the buyers’ loan does not pull through on time, there is no need to worry. We have encountered these problems before so we know how to handle them efficiently and in a stress-free manner.</p>

	<h3>Closing.</h3>
	<p>The closing agent will furnish all parties involved with a settlement statement, which summarizes and details the financial transactions enacted in the process. The buyer(s) will sign this statement and then you will sign as well as the closing agent, certifying its accuracy. If you are unable to attend the scheduled closing, then arrangements can be made depending on the circumstances and the notice that we receive. If you are receiving funds from the transaction, you can elect to either have the funds wired electronically to an account at your financial institution, or have a check issued to you at the closing. The seller should arrange to have all property keys and any other important information for the new purchaser at the closing, so that the purchaser may receive these items at this time.</p>
@endsection