@extends('pages.buyers.buyers')
@section('buyers-content')
	<h3>Closing Day</h3>
	<p>If you have come this far, then this means that it is almost time for a congratulations, but not yet. Do not forget to tie up these loose ends:</p>

	<h3>Final Walk-Through Inspection.</h3>
	<p>More of a formality than anything else, the final inspection takes place a day before, or the day of the closing. You will visit the property to verify that all is in working order, everything is the same as when you last viewed the property, that there are no extra items left behind, and that everything included in your purchase is still at the property.</p>

	<h3>Home Services and Utilities.</h3>
	We will provide a list of useful numbers for the activation of home services and utilities after the closing occurs.

	<h3>Be Prepared.</h3>
	<p>We are ready to assist you should an unforeseen glitch pop up, even at this last stage. Something at the property breaks down, or some other minor detail – no need to worry. We have encountered these problems before so we know how to handle them efficiently and in a stress-free manor.</p>

	<h3>Closing.</h3>
	<p>The closing agent will furnish all parties involved with a settlement statement, which summarizes and details the financial transactions enacted in the process. You and the seller(s) will sign this statement, as well as the closing agent, certifying its accuracy. If you are obtaining financing, you will have to sign all pertinent documentation required by the lending institution. If you are unable to attend the scheduled closing, arrangements can be made depending on the circumstances and the notice that we receive. If you are bringing funds to the transaction, you can elect to either have the funds wired electronically into the closing agent’s escrow account, or bring a certified bank check to the closing in the amount specified on the settlement statement. The seller should arrange to have all property keys and any other important information for you at the closing so that you may receive these items at this time.</p>
@endsection