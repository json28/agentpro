@extends('pages.buyers.buyers')
@section('buyers-content')
	<p>Purchasing a property is most likely the biggest financial decision you will ever make. Whether this is your first purchase or you are an experienced buyer, this decision must be made carefully</p>

	<h3>Why Do You Want To Buy?</h3>
	<p>Are you tired of paying rent? Have you decided to pay your own mortgage and not your landlord’s? Have you outgrown your current home? Are you looking for an investment portfolio? Are you looking for a rental property? Would you like a larger yard? Would you rather live in a different area? Do you want to shorten your commute? Having a clear sense of your reasons for buying will help you choose the right property.</p>

	<h3>Has Your Income Grown?</h3>
	<p>Property ownership is an excellent investment; whether you are looking for your dream home, a rental property, or to expand your investment portfolio. Owning real estate is one of the least risky ways to build equity or to obtain a greater return on your initial investment.</p>
@endsection