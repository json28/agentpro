@extends('pages.buyers.buyers')
@section('buyers-content')
	<p>Once those preparations are out of the way, it is time to find the right property for you.</p>

	<h3>Take a Drive.</h3>
	<p>Get to know the neighborhoods, complexes, or subdivisions, which interest you. Drive around and get a feel for what it would be like to own a property in the area. Start getting a sense of the properties available in those areas.</p>

	<h3>Narrow Your Search.</h3>
	<p>Select a few properties that interest you the most and have your real estate agent make appointments to visit them. Ask your real estate agent about the potential long term resale value of the properties you are considering.</p>

	<h3>Time to Buy.</h3>
	<p>Once you have picked out the property you want to purchase, your real estate agent can help you make an offer that the seller will accept. A good agent will investigate the potential costs and expenses associated with the new property. An agent can also help you draft your offer in a way that gives you the advantage over another offer.</p>
@endsection