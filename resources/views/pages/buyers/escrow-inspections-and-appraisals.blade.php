@extends('pages.buyers.buyers')
@section('buyers-content')
	<h3>The Process, Step-by-Step</h3>
	<h3>The Initial Agreement and Deposit.</h3>

	<p>An effective agreement is a legal arrangement between a potential purchaser and the property’s seller.</p>

	<p>Some important tips to keep in mind to streamline the process:</p>

	<ul>
		<li>Keep written records of everything. For the sake of clarity, it will be extremely useful to transcribe all verbal agreements including counter-offers and addendums and to convert them into written agreements to be signed by both parties. We will assist you in drafting all the paperwork for your purchase and make sure that you have copies of everything.</li>
		<li>Stick to the schedule. Now that you have chosen your offer, you and the seller will be given a timeline to mark every stage in the process of closing the real estate contract. Meeting the requirements on time ensures a smoother flow of negotiations so that each party involved is not in breach of their agreements. During the process we will keep you constantly updated, so you will always be prepared for the next step.</li>
	</ul>

	<h3>The Closing Agent.</h3>
	<p>Either a title company or an attorney will be selected as a closing agent. The closing agent will hold the deposit in escrow and will research the complete recorded history of the property to ensure that the title is free and clear of encumbrances by the date of closing and that all new encumbrances are properly added to the title. Some properties are subject to restrictions which limit various activities such as building or parking restrictions. There may be recorded easements and encroachments, which limit the rights to use your property.</p>

	<h3>How to Hold Title.</h3>
	<p>You may wish to consult an attorney or tax advisor on the best way to hold title. Different methods of holding title have different legal, estate and tax implications, especially when selling or upon death of the title holder.</p>

	<h3>Inspections.</h3>
	<p>Once your offer is accepted by the seller, you will need to have a licensed property inspector inspect the property within the time frame that was agreed upon in the effective contract to purchase. You may elect to have different inspectors inspect the property, if you wish to obtain professional opinions from inspectors who specialize in a specific area (eg. roof, HVAC, structure). If you are purchasing a commercial property, then you will need to have an environmental audit done on the site for the lending institution. We can recommend several different inspectors.</p>

	<p>Depending on the outcome of these inspections, one of two things may happen:</p>

	<p>1.	Either each milestone is successfully closed and the contingencies will be removed, bringing you one step closer to the close, or</p>

	<p>2.	The buyer, after reviewing the property and the papers, requests a renegotiation of the terms of contract (usually the price).</p>

	<h3>Appraisal and Lending.</h3>
	<p>It is imperative that you keep in close communication with your lender, who will let you know when additional documents are needed to approve your loan application and fund your loan. If the agreement is conditional upon financing, then the property will be appraised by a licensed appraiser to determine the value for the lending institution, via a third party. This is done so that the lending institution can confirm their investment in your property is accurate. Appraisers are specialists in determining the value of properties, based on a combination of square footage measurements, building costs, recent sales of comparable properties, operating income, etc. When you are within two weeks of closing, double check with your lender to be sure the loan will go through smoothly and on time.</p>

	<h3>Association Approval.</h3>
	<p>If the property that you are purchasing is conditional upon an association approval, request the rules, regulations, and other important documents from the seller as soon as you have an effective agreement to purchase. Make sure that the application documents and processing fees are submitted to the appropriate person at the association by the required time. Fill out all of the information completely and legibly so there is no delay in processing the application. If you are required to meet with the association for your approval, make an appointment as soon as possible for the interview. Most associations require a certificate of approval before move-in. Your closing agent will request that the original copy of this approval letter be brought to the closing, so that it can be recorded with the deed in the county public records.</p>

	<h3>Property Insurance.</h3>
	<p>If you are obtaining a loan, you will be required by your lender to purchase a certain amount of insurance on the property. The value will depend on the lending institution and the purchase price of the property. You may be able to save hundreds of dollars a year on homeowners insurance by shopping around for insurance. You can also save money with these tips.</p>

	<ul>
		<li><strong>Consider a higher deductible.</strong> Increasing your deductible by just a few hundred dollars can make a big difference in your premium.</li>
		<li><strong>Ask your insurance agent about discounts.</strong> You may be able get a lower premium if your home has safety features such as dead-bolt locks, smoke detectors, an alarm system, storm shutters or fire-retardant roofing materials. Persons over 55 years of age or long-term customers may also be offered discounts.</li>
		<li><strong>Insure your house NOT the land under it.</strong> After a disaster, the land is still there. If you do not subtract the value of the land when deciding how much homeowner’s insurance to buy, you will pay more than you should.</li>
	</ul>

	<p>We will be happy to recommend experienced knowledgeable insurance agents for every property type.</p>
@endsection