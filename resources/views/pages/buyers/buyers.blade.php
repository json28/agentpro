@extends('layouts.default')
@section('page-content')
	<div class="buyers-header row">
		<div class="col-2">
			@if($buyerStep == 1)
			<a class="active">
			@else
			<a href="/buyers/deciding-to-buy">
			@endif
				<i class="ai-icon-1"></i>
				<span>deciding to buy</span>
			</a>
		</div>
		<div class="col-2">
			@if($buyerStep == 2)
			<a class="active">
			@else
			<a href="/buyers/preparing-to-buy">
			@endif
				<i class="ai-icon-2"></i>
				<span>preparing to buy</span>
			</a>
		</div>
		<div class="col-2">
			@if($buyerStep == 3)
			<a class="active">
			@else
			<a href="/buyers/choosing-a-real-estate-agent">
			@endif
				<i class="ai-icon-3"></i>
				<span>choosing a real estate agent</span>
			</a>
		</div>
		<div class="col-2">
			@if($buyerStep == 4)
			<a class="active">
			@else
			<a href="/buyers/time-to-go-shopping">
			@endif
				<i class="ai-icon-4"></i>
				<span>time to go shopping</span>
			</a>
		</div>
		<div class="col-2">
			@if($buyerStep == 5)
			<a class="active">
			@else
			<a href="/buyers/escrow-inspections-and-appraisals">
			@endif
				<i class="ai-icon-5"></i>
				<span>escrow inspections and appraisals</span>
			</a>
		</div>
		<div class="col-2">
			@if($buyerStep == 6)
			<a class="active">
			@else
			<a href="/buyers/moving-in">
			@endif
				<i class="ai-icon-6"></i>
				<span>moving in</span>
			</a>
		</div>
	</div>

	@yield('buyers-content')
@endsection