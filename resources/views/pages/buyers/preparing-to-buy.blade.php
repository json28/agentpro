@extends('pages.buyers.buyers')
@section('buyers-content')
	<p>Before you start shopping for your property, it is a good idea to make some preparations.</p>

	<h3>Build Your Green File.</h3>
	<p>A green file contains all your important financial documents. You will need it to secure financing for your property. The typical green file should contain:</p>

	<ul>
		<li>Financial statements</li>
		<li>Bank accounts</li>
		<li>Investments</li>
		<li>Credit cards</li>
		<li>Auto loans</li>
		<li>Recent pay stubs</li>
		<li>Tax returns for two years</li>
		<li>Copies of leases for investment properties</li>
		<li>401K statements, life insurance, stocks, bonds, and mutual account information.</li>
	</ul>

	<h3>Check Your Credit Rating.</h3>
	<p>Your credit score will have a huge impact on what type of property you can buy, and at what price. It is first recommended to check your credit rating with an experienced lending institution so that we can determine what you can afford. The lender will research your credit ratings from the three credit reporting agencies Equifax, Experian and Trans Union. We will be happy to recommend experienced, knowledgeable lenders in the residential, construction, and commercial and investment real estate fields.</p>

	<h3>Be Careful With Your Finances.</h3>
	<p>Now is not a good time to make sudden career changes or large purchases. You want to approach your property purchase from a position of financial stability.</p>
@endsection