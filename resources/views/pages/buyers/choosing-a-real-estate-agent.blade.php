@extends('pages.buyers.buyers')
@section('buyers-content')
	<p>Buying a property requires making many important financial decisions, understanding complex issues and completing a lot of paperwork. It helps to have an expert in your corner when undertaking such a large purchase. We can guide you through this process, and also provide you with access to property listings before they hit the general market.</p>

	<p>Here are some factors to consider when choosing your real estate professional:</p>

	<ul>
		<li>Look for a full-time agent – one who has experience completing transactions similar to yours.</li>
		<li>Interview a few agents: Are they familiar with the area in which you are interested?</li>
		<li>Ask how much time the agent will have for you, and if they are available at night and on weekends.</li>
		<li>Ask about their credentials and education: A good agent will continually strive to improve and gain knowledge of the latest real estate trends and hold the highest designations in their respective fields of expertise.</li>
		<li>Does the agent return your calls promptly? Time is money when attempting to buy a property.</li>
		<li>Ask for a list of properties they have sold or a list of references.</li>
		<li>Choose an agent who listens attentively to your needs and concerns. Pick an agent, with whom you feel comfortable.</li>
	</ul>
@endsection