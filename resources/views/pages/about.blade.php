@extends('layouts.default')
@section('page-content')
	<p>Maxwell Mosley not only represents homebuyers in New York, he lives there. That gives him a decided advantage over other agents who live in other parts of New York. His knowledge and experience when it comes to buying and selling real estate in New York is simply unmatched.</p>

	<p>His thorough familiarity and knowledge of New York’ communities, neighborhoods, and local attractions helps him match his clients with the right home for them. Maxwell Mosley is able to give his clients detailed information about the pluses and minuses of each area. Plus, as a certified relocation expert, he stays on top of the latest changes and trends in both the local and national real estate markets.</p>

	<p>His educational background, combined with his many years of experience, is what makes him a trusted agent. His professionalism, winning personality, and personalized service are just what buyers need to find the homes of their dreams.</p>

	<p>In the course of five years he has earned his reputation as a driven agent who gives his clients a smooth and stress-free home buying and selling experience. He understands that every client is different and will work tirelessly to help them achieve their real estate goals. There is no one-size-fits-all method for representing his clients in their real estate transactions.</p>

	<p>Part of what sets him apart from his competitors is his ability to listen carefully to the unique needs, desires, and goals of each of his clients. It is the personal approach borne out of that deep understanding that makes him a leading agent in New York.</p>
@endsection