@extends('layouts.default')
@section('page-content')
<style type="text/css">

.list-wrapper {
	border: 1px solid #ccc;
	float: left;
	margin-top: 30px;
	display: none;
}
	
	ul.table-hdr {
		width: 100%;
		background: #ededed;
		border-bottom: 1px solid #ccc;
		margin: 0 !important;
		padding: 12px 1.5%;
	}

		ul.table-hdr li {			
			font-weight: 700;
			font-size: 14px;
			color: #464646;
			
		}

		ul li.one, ul li div.prop-img {width: 27%; display: inline-block;}
		ul li.two, ul li div.prop-add {width: 17%; display: inline-block; line-height: 19px;}
		ul li.three, ul li div.list-price {width: 14%; display: inline-block;}
		ul li.four, ul li div.beds {width: 7%; display: inline-block; text-align: center;} 
		ul li.five, ul li div.baths {width: 7%; display: inline-block; text-align: center;}
		ul li.six, ul li div.area {width: 12%; display: inline-block;}
		ul li.seven, ul li div.sold-date {width: 12%; display: inline-block;}

		ul li.two, ul li div.prop-add a {
			color: inherit;
			text-decoration: none;
		}

		.list-wrapper .table-list .list-price p.list-price-sec{
			margin: 0 !important;
		}
		div.list-price p{
			font-weight: 700;
		}
	ul.table-list {
		width: 100%;
		margin: 0 auto !important;
		padding: 0;
	}

		ul.table-list li {
			width: 100%;
			display: inline-block;			
			font-size: 15px;
			font-weight: 300;
			color: #464646;
			
			border-bottom: 1px solid #ccc;
			padding: 15px 1.5%;
		}

			ul.table-list li div {
				vertical-align: top;
				height: auto;
			}

				ul.table-list li div label {display: none; font-weight: 700;}

			ul.table-list li div.prop-img {position: relative; overflow: hidden;}

			ul.table-list li div .sold-icon,
			ul.table-list li div .sale-icon,
			ul.table-list li div .rent-icon,
			ul.table-list li div .lease-icon,
			ul.table-list li div .pending-icon,
			ul.table-list li div .exclusive-icon {
				position: absolute;
				top: 0;
				left: 0;
				width: 40%;
			}

			ul.table-list li img.main-img {
				max-width: 98%;
				height: auto;
			}

ul.list{
	width: 100%;
	float: left;
	margin: 0 !important;
	/*font-size: 0;*/
	
	color: #575757;
}
	ul.list li{
		display: block;
		width: 100%;		
		padding: 30px 0;
		border-top: 1px solid #cfcfcf;
	}
		ul.list li:first-child{
			padding-top: 40px;
			border: 0;
		}

		ul.list li .prop-img-wrap{
			display: inline-block;
			vertical-align: top;
			width: 38.8%;
			position: relative;
		}
		.property_type .prop-img-wrap img {
		    width: 100%;
		}
			.property_type .prop-img-wrap .sold-icon{
				width: 30% !important
			}

			.prop-img-wrap .sold-icon {
				position: absolute;
				z-index: 9;
				top: -20px;
				left: -20px;
			}

		 	#content-listings ul.list li .prop-img-wrap img.main-img{
				width: 100%;
				max-width: initial !important;
				margin: 0;
			}

		ul.list li .prop-det{
			width: 55.2%;
			display: inline-block;
			vertical-align: top;
			margin-left: 3%;
		}
		ul.list li .prop-det .prop-title{
			font-weight: 700;
			font-size: 24px;
			line-height: 24px;
			float: left;
			width: 65%;
		}

		ul.list li .prop-det .prop-title a {
			text-decoration: none;
			color: inherit;
		}

		ul.list li .prop-det p.list-price {
			float: right;
			width: 32%;
			font-size: 24px;
			color: #5d5d5d;
			margin: 0 !important;
			line-height: 27px !important;
			font-weight: 700;
			text-align: right;
		}

		ul.list .prop-det .prop-beds {
			width: 100%;
			float: left;
			position: relative;
			border-bottom: 1px solid #ececec;
			border-top: 1px solid #ececec;
			padding: 13px 10px;
			margin: 20px 0 10px;
			font-weight: 700;
			font-size: 15px;
			line-height: 19px;
			color: #5d5d5d;
		}

			ul.list .prop-det .prop-beds em { 
				font-size: 18px;
				color: #bababa;
			}

			.prop-det .prop-beds .det-smi {
				position: absolute;
				top: 0;
				right: 5px;
			}

				.prop-det .prop-beds .det-smi .a2a_kit_size_32 {
					display: block;
					top: 8px;
					position: relative;
				}

				.prop-det .prop-beds .det-smi .a2a_kit_size_16 {
					display: block;
					top: 16px;
					position: relative;
				}


				.prop-det .prop-beds .det-smi img {
					margin-left: 10px;
				} 

				.prop-det .prop-beds .det-baths {
					margin-left: 15px;
					color: #5d5d5d;
				}

				.prop-det .prop-beds .border {display: inline-block; margin: 0 10px; color: #dadada; font-size: 28px;}

				.prop-det .prop-beds .det-sqft {					
					float: none;
					color: #5d5d5d;
					border-left: 1px solid #cccccc;
					padding-left: 15px;
					margin-left: 15px;
				}

		ul.list li p.prop-desc {
			color: #5f5f5f;
			font-weight: 300;
			font-size: 13px;
			line-height: 22px !important;
			width: 100%;
			float: left;
			margin: 0 !important;
			text-align: justify;
		}

		ul.list li .prop-det .view-details{
				display: block;
				width: 113px;
				height: 29px;
				background: #6a6a6a;
				box-sizing: border-box;
				font-size: 14px;
				color: #fff;
				text-transform: uppercase;
				text-align: center;
				padding: 3px 0;
			    margin-top: 13px;
			    float: left;
			    line-height: 23px;
			}

				ul.list li .prop-det .view-details:hover {
					opacity: 0.8;
				}
</style>
	<div class="row post-single">
		<ul class="list">
		@foreach ($propert as $properts)
			<li>
			  <div class="prop-img-wrap">
					<a class=" listing-item" data-listing-id="181" href="">
						<img width="324" height="224" src="uploads/images/{{ $properts->photo }}" class="main-img wp-post-image" alt="">
							</a>
						</div>
								<div class="prop-det">
									<span class="prop-title"><a class=" listing-item" data-listing-id="181" href="https://agentpro-montecarlo.agentimage.com/listing/palm-springs"> {{ $properts->name }} </a></span>
										<div class="prop-beds">
											<em class="ai-bed"></em> {{ $properts->beds }} <span class="det-baths">
											<em class="ai-showers"></em>  {{ $properts->bath }} </span> 
												
											<div class="det-smi">
												
											</div>
										</div>
									<p class="prop-desc">
									{{ strip_tags(html_entity_decode($properts->description)) }}</p>	
								<a class="view-details  listing-item" data-listing-id="181" href="/properties/{{ $properts->id }}">Details</a>
							</div>								
						</li>
				@endforeach
			</ul>
		</div>
@endsection