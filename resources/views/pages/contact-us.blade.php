@extends('layouts.default')
@section('page-content')
	<div class="row" id="contact-us">
		<div class="col-md-6">
			<div class="form-title">
				<h4>We would love to hear from you!</h4>
				<span>Send us a message and we’ll get right back in touch.</span>
			</div>
			<div class="form-contacts">

				@foreach ($agentinfo as $agentinfos)
				<!-- <h3>AgentImage</h3> -->
				<img src="uploads/images/{{ $agentinfos->agent_photo }}" width="400">
				<div><em class="ai-font-phone contact-icon"></em>{{ $agentinfos->phone }}</div>
				<div><em class="ai-font-envelope contact-icon"></em><a href="#">{{ $agentinfos->email }}</a></div>
				@endforeach
			</div>
			<p>Required fields are marked *</p>

			<div class="col-md-6">

				 @if (count($errors) > 0)
			        <div class="alert alert-danger">
			            <strong>Whoops!</strong> There were some problems with your input.<br><br>
			            <ul>
			                @foreach ($errors->all() as $error)
			                    <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    @endif

			     @if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            <p>{{ $message }}</p>
			        </div>
			    @endif

				<div class="row m-b-10">
					<input class="col-12" type="text" name="fname" placeholder="First Name *">
				</div>
				<div class="row m-b-10">
					<input class="col-12" type="text" name="lname" placeholder="Last Name *">
				</div>
				<div class="row m-b-10">
					<input class="col-6" type="email" name="email" placeholder="Email Address *">
					<input class="col-6" type="text" name="phone" placeholder="Phone Number *">
				</div>
				<div class="row m-b-10">
					<textarea class="w-100" name="comments" placeholder="Additional Comments"></textarea>
				</div>
				<div class="row m-b-10">
					<button type="submit" class="w-100">SEND</button>
				</div>
			</div>



		</div>
	</div>
@endsection