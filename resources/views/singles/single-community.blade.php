@extends('layouts.default')
@section('page-content')
	<div class="row post-single">
		<div class="col-10">
			<img class="w-100" src="/images/communities/{{ $community['slug'] }}.jpg">
			<p class="post-content">{{ $community['content'] }}</p>
		</div>
	</div>
@endsection