@extends('layouts.default')
@section('page-content')
	@foreach($communities as $community)
	<article class="row post-archive">
		<div class="col-md-4 post-image">
			<img class="w-100" src="/images/thumbnails/{{ $community['slug'] }}.jpg">
		</div>
		<div class="col-md-8 post-details">
			<h2 class="post-title"><a href="/communities/{{ $community['slug'] }}">{{ $community['title'] }}</a></h2>
			<p class="post-content">{{ $community['content'] }}</p>
			<a class="read-more" href="/communities/{{ $community['slug'] }}">Read more</a>
		</div>
	</article>
	@endforeach
@endsection