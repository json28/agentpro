<!DOCTYPE html>
<html lang="en">
@include('includes.head')

<body>
@yield('template-content')

    @include('includes.scripts')

</body>
</html>